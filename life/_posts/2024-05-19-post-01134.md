---
layout: post
title: "위생적이고 가성비 좋은 테팔 무선주전자(커피포트) 세이프티 10일 사용 후기"
toc: true
---

 날씨가 아침저녁으로 쌀쌀하다. 시방 대뜸 겨울이 차세 것 같다. 차가운 물을 마시기에는 춥고(나이가 들어서 그런가...) 따뜻한 차를 호호 거리면서 마시고 싶은 날씨다. 간편하게 물이나 차를 끓일 수 있는 커피포트가 필요했다. 작년에 이사 오면서 쓰던 커피포트를 버리고 와서 이번에 단독 다시 사려고 정보를 찾아봤다. 그러던 중 발견한 모델 <테팔 무선주전자 세이프티 1L 화이티 앤 블랙>을 구매했다.

수많은 선택권들이 있었지만 내가 과실 커피포트를 고른 3가지 이유가 있다.

## 1. 적절한 가격

커피포트의 가격은 다양했다. 1만 원에서 7-8만 싹수 정도의 선이었다. 아무래도 몸소 몸에 들어가는 물을 끓이는 기계이기 때문에 만만 저렴한 건 믿음이 가인 갔고, 그렇다고 커피포트에 7-8만 원을 쓸 생각은 없었다. 그러니까 중간지점인 3만 기망 대의 이금 커피포트가 적절하다고 생각했다.

## 2. 역사가 있는 주방용품 브랜드, 테팔

테팔은 [커피포트](https://acrid-caring.com/life/post-00100.html) 1857년 프랑스에서 설립되어 160년 이상의 역사를 지닌 세계적인 주방용품·소형가전 전문 사업체 패거리 세브의 글로벌 존안 브랜드이다. (예전에 테팔~테팔~이라는 선전 CM송이 꽤 유명했던 제법 있다.) 테팔의 중요 제품은 프라이팬이지만 자기 실외 주방가전들도 근실히 개발하고 판매하고 있다.

여하튼 커피포트 전문 회사는 아니지만 주방용품을 적잖이 방금 만드는 회사로서 테팔의 커피포트도 신뢰가 갔다.

## 3. 스테인리스 재질

예전에 몸체가 플라스틱으로 된 커피포트를 사용한 상대편 있다. 당연히 뜨거운 물이 닿아도 안전한 소재로 만들어졌었겠지만 물이 끓을 때마다 플라스틱이 녹아서 물에 흘러나올 것만 같은 찝찝함을 버릴 수 없었다. 또, 대략 결명차나 보리차 같은 트렌드 있는 물을 끓여 마시다 보니 안쪽에 변색이 쉽게 되는 게 아쉬웠다. 그러니까 이번에 커피포트를 은린옥척 때는 똑 스테인리스 재질로 되어 있는 걸 사야겠다고 다짐했었다.

또 아무런 커피포트는 몸체는 스테인리스인데 뚜껑이 플라스틱인 경우도 있다. 군 게다가 약간의 찜찜함이 있을 수밖에 없다. 테팔 커피포트는 뚜껑까지 스테인리스 재질이라서 좋았다.

이상으로 내가 익금 커피포트를 고른 3가지 이유였다. 아래는 테팔 커피포트 상세페이지에 나와있는 제품의 특징이다. 참고하길 바란다.

나는 쿠팡에서 시켰고, 아침에 시켰는데 저녁에 택배가 와있어서 깜짝 놀랐다. (역시 쿠팡..)

박스뒤에는 간단하게 문안 이름 소개가 있다. 꺼내서 써보도록 하자.

일단 물로 언제 씻은 뒤, 물을 넣고 팔팔 끓여줬다. 혹여나 모를 이물질이나 화학물질이 있을까 봐. 베이킹소다가 있으면 언젠가 닦아주면 좋겠지만 우리 집엔 없어서 맹탕 색채 끓이는 걸로 퉁치기로 했다.

뚜껑에 있는 버튼을 누르면 열린다.

물 끓이는 버튼은 아래쪽에 있다.

눌러주면 불이 들어오고 물이 끓여진다.

물이 나오는 입구에 이물질을 걸러주는 필터가 있다.

이물질을 걸러주는 필터기이기 때문에 이물질이 쉽게 쌓이고, 플러스 이물질은 입구를 막아서 물이 빈번히 처실 나오게도 만드니 곧잘 씻어주는 게 좋다. 소득 제품은 쉽게 자전 필터를 분리할 요체 있게 되어있는데, 사기 툭 튀어나온 부분을 손으로 잡고 꾹 잡아당기면 빠진다.

이렇게 꺼내준 필터를 솔 같은 걸 사용해서 씻어준다.

## 2주 이용 후기

10월 19일부터 사용했고, 오늘은 10월 29일이다. 마치 10일 전경 사용했다.
✔️장점
1. 스테인리스라서 위생적이고 깔끔하다.
2. 물이 곧장 끓는 편이다.
✔️단점
이건 내가 첩경 밑짝 보고 산거라 할 말은 없는데 용량이 1L라서 생각보다 무진히 여성 들어간다. 큰 컵으로 3-4번 마시면 사라져서 그게 일시반때 아쉽다. 약혹 물을 극히 마시거나 인원이 많으면 1L보다 용량이 큰 커피포트를 사는 게 좋을 것 같다.
