---
layout: post
title: "한국 주식 시장에서 알아야할 7가지 필수 용어!!"
toc: true
---


 

#### 주식 시장에 투자할 때, 일반적으로 사용되는 몇 분지 필수 용어를 이해하는 것이 중요하다.

#### 다음은 한국 주식 시장의 주식과 관련된 일곱 나뭇가지 중요 용어입니다:)
 

 

## 

## 1. 코스피와 코스닥
 

  코스피, 또 종합주가지수는 한국의 주요 주식 시장 지수이다. 삼성전자, 현대차, SK하이닉스 등 유족히 알려진 기업들이 대거 포함된 한국거래소에 상장된 쥔아저씨 크고 유동성이 높은 기업들의 실적을 추적하는데, 코스피는 오랜 핵심 영업을 해온 우량 기업들이 많아 더욱 안정적이고 보수적인 지수로 평가된다.
 반면에 코스닥은 1996년에 설립된 새롭고 작은 시장 지수이다. 그것은 많은 수가 기술과 생명공학 분야에 있는, 규모가 시적 설립이 덜 된 기업들의 성과를 추적한다. 코스닥은 이들 기업이 상금 점진 단계에 있고 실적이나 안정적인 이문 흐름을 확립하지 못했을 요체 있기 그러니까 시시로 훨씬 변동성이 크고 위험한 지수로 간주된다.
 종합적으로 보면 코스피는 좀 우극 확립되고 보수적인 시장지수인 그러나 코스닥은 성장기업 중심으로 사화 위험성이 높은 지수다. 투자자들은 투자 목표와 누란지세 허용도에 따라 일편 내지 두 지수 모두에 투자할 복 있다.
 

##    2. 시가총액
 

  시가총액은 회사의 출간 주식의 총가치이며, 목하 주가에 간행 주식 수를 곱하여 계산됩니다. 따라서, 제한 회사가 100주를 가지고 있고 교량 주식의 가격이 10달러라면, 노형 회사의 시가총액은 1,000달러가 될 것이다. 시가총액은 회사의 규모와 가치에 대한 중요한 지표이며, 같은 공업 밑면 회사를 비교하는 데 사용될 수 있다. 대형 대기업의 시가총액은 10조 염원 이상, 중형 기업의 시가총액은 1조 원에서 10조 낙관 사이이며, 꼬맹이 대기업의 시가총액은 1조 포부 미만이다.
 

  주의해야 할 점은 시가총액은 주식의 가격과 적용 가능한 주식의 수에 따라 달라질 호운 있기 그러니까 고정된 숫자가 아니라는 것입니다.
 

##    3. 주가 수익 비율 (PER) - price-to-earnings ratio

 

  가격 채비 수입금 비율(PER)은 회사의 당금 주가를 주당 순이익(EPS)과 비교하는 재무 비율이다. 그것은 투자자들이 기업체 수익의 다리갱이 원금에 대해 지불할 의향이 있는 금액을 측정하는 논평 비율이다. 더 높은 PER은 투자자들이 하지 수익에 대해 훨씬 많은 돈을 지불할 의향이 있음을 나타내며, 이는 회사에 대한 높은 개진 기대치의 신호가 될 삶 있다. 그러나, 높은 PER은 역 주식이 과대평가되고 위험한 투자가 될 무망지복 있다는 것을 의미할 운 있다.
 

  예를 들어, 가게에서 사탕 계한 개을 사는 것처럼 생각해 보자. 사탕의 가격은 네년 회사의 주식 1주의 가격을 나타낸다. 당신이 받는 사탕의 양은 회사의 수입을 나타냅니다. 만일 사탕의 가격이 2천 원이고 당신이 500원 가치의 사탕을 받는다면, PER은 4가 될 것이다. (즉, 당신은 주식 한도 주를 저기 위해 회사 수익의 4배를 지불하는 것이다.) PER이 높을수록 투자자들은 회사가 벌어들이는 1천원에 대해 훨씬 많은 돈을 지불할 의사가 있다는 것을 의미하며, 이는 회사의 미래 수입 잠재력에 대한 높은 기대를 의미할 삶 있다. 낮은 PER은 투자자들이 회사의 앞날 수익에 대해 덜 낙관적이라는 것을 의미할 목숨 있다.
 

##   4. 자기자본수익률 (ROE) - Return on Equity
 

  자기자본수익률(ROE)은 회사가 주주 지분의 다리 원당 하 많은 이익을 창출하는지를 측정하는 재무 비율이다. 그것은 순이익을 주주 지분으로 나누어 계산된다. ROE는 회사의 수익성과 효율성에 대한 중요한 지표이며, 같은 공 최하층 회사를 비교하는 데 사용될 생목숨 있다. 높은 ROE는 회사가 주주들의 투자에 대한 좋은 수익을 창출하고 있음을 나타낸다.
 

 

  예를 들어, 당신이 레모네이드 가판대를 가지고 있다고 상상해 보자. 당신은 스탠드와 재료를 사기 위해 10,000원을 투자했다. 하시 내처 당신의 판매대는 1,000원의 이익을 낸다. 귀사의 ROE는 10%(이익 1,000원을 초년 투자액 1만 원으로 나눈 값)가 된다.
 마찬가지로, 기업의 하소연 ROE는 순이익(이익)을 주주 자본으로 나누어 계산합니다. 예를 슬쩍하다 기업의 순이익이 10억원, 주주자본이 100억 원이라면 ROE는 10%(주주가 투자한 1원당 10%의 수익을 얻고 있음을 의미)가 된다.
 ROE는 기업이 이익을 창출하기 위해 주주들의 투자를 오죽이나 효율적으로 사용하고 있는지를 보여주기 그리하여 중요한 지표이다. 일반적으로 ROE가 높을수록 주주들이 투자한 원화당 이익을 훨씬 적잖이 낸다는 것을 의미하기 그렇게 보다 나은 것으로 간주된다. 그럼에도 불구하고 ROE의 허락 범위는 산업별로 다를 수명 있기 그리하여 동일한 공 기수 다른 기업의 ROE를 비교하는 것이 중요합니다.

##   5. 배당 수익률
 

  배당 수익률은 투자자들이 회사가 지불한 배당금에서 받는 수익률을 측정하는 재무 비율이다. 그것은 주당 연간 배당금을 현세 주가로 나누어 계산된다. 배당 수익률은 투자에서 정기적인 수입을 찾는 투자자들에게 중요한 요소이다. 높은 배당 수익률은 더욱이 회사가 안정적이고 수익성이 있다는 것을 나타낼 요체 있다.
 

  예를 들어, 당신이 주당 1,000원의 연간 배당금을 지급하는 회사의 주식을 1주 가지고 있다고 상상해 보라. 주가가 5만 원이라면 배당수익률은 2%(연간 배당금 1000원을 주거 5만 원에 100을 곱한 금액)가 된다.
마찬가지로 기업의 경우 연간 주당 배당금을 100에 곱한 주가로 나눠 배당수익률을 계산한다. 예를 들어 기업이 주당 연간 배당금 500원을 지급하고 주가가 2만5000원이라면 배당수익률은 2%(배당 형태로 주주에게 2%의 수익률을 지급하고 있다는 의미)가 된다.
배당수익률은 투자로부터 정기적인 수입을 받는 데 관심이 있는 투자자들에게 중요한 지표이다. 일반적으로 배당 수익률이 높을수록 투자 수익률이 높다는 것을 의미하기 왜냐하면 가일층 나은 것으로 간주된다. 오히려 배당수익률만을 기준으로 투자를 결정하기 전에 회사의 재무건전성과 다음날 사망 등 다른 요소들을 고려하는 것이 중요하다.
 

 

##   6. 베타(Beta)
 

  베타는 주식 시장에서 투자자들이 적극적으로 활용하는 기준점 중가운데 하나이다. 간단히 말하자면, 전체 시장에 비해 주식의 변동성을 측정하는 척도이다. 1의 베타는 주식이 시장에 따라 움직인다는 것을 의미하는 반면, 1보다 큰 베타는 주식이 시장보다 더더욱 불안정하다는 것을 [주식 시장](https://lunchfar.com/life/post-00041.html) 의미한다. 베타가 1 미만이라는 것은 주식이 시장보다 변동성이 적다는 것을 의미한다. 베타는 주식의 위험성의 중요한 지표이며, 포트폴리오의 위험과 수익을 평가하는 데 사용될 무망지복 있다.
 

  예를 들어, 벽 회사의 주가가 일반적인 시장의 변동과 대부분 동일한 비율로 움직인다면, 이놈 회사의 주식의 베타 값은 1.0이다. 약혹 이년 주식이 일반적인 시장보다 변동성이 낮게 움직인다면, 그대 주식의 베타 값은 1.0보다 작을 것이고, 반대로 일반적인 시장보다 변동성이 높게 움직인다면, 너 주식의 베타 값은 1.0보다 높을 것이다.
 

  주식 투자에서 베타 값은 풍부히 중요하다. 투자자는 피차일반 일반적인 시장과 어째 밀접하게 연관되어 있는 기업의 주식에 투자를 하면 좋을지 결정할 단시간 베타 값을 고려한다. 여혹 투자자가 안정적인 수익을 원한다면, 일반적인 시장과 밀접하게 연관된 기업의 주식을 선택하는 것이 좋을 것이다. 반면에 높은 수익을 원한다면, 일반적인 시장보다 더더욱 높은 변동성을 가진 주식을 선택해야 할 것이다.
 

  한 수지 주의할 점은, 베타 값이 높다고 해서 상상 높은 수익이 보장되는 것은 아니다. 투자자는 상상 계열 기업의 재무상태와 다음날 전망을 함께 고려하여 투자 결정을 내려야 한다.
 

## 7. 거래량
 

  거래량은 하루 혹은 일정 길이 거리 거래되는 총 주식 수이다. 높은 거래량은 주식에 상당한 관심이 있고 적극적으로 거래되고 있음을 나타낼 행복 있으며, 이는 주식을 일층 쉽게 작정 팔 생목숨 있다. 낮은 거래량은 주식을 사고파는 것을 어렵게 만들 핵심 있으며, 더더군다나 주식에 대한 관심이 거의거의 없다는 것을 나타낼 행우 있다.
 

##   익금 일곱 줄거리 뼈대 용어를 이해하면 투자자들이 한국 주식 시장에 투자할 동시대 정보에 입각한 결정을 내리는 데 도움이 될 수 있다.

