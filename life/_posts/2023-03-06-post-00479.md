---
layout: post
title: "메타버스의 미래"
toc: true
---

 작년에는 메타버스와 같은 신기술에 열광하며 많은 기대감을 가졌으나 캠페인 침체는 이러한 열기를 식히기에 충분했다. 이러한 열기가 식으면서 일삽시 더한층 냉철한 시각으로 메타버스의 미래에 대해서 이야기를 나눠보고자 한다.

 

## 현재의 메타버스
 2021년은 로블록스, 페이스북, 엔비디아 등 대다수 기업이 메타버스 투자 및 개발 계획을 발표하며 시장은 과열이 되었고 2022년에는 진정세를 보이며 관련 종목의 도로 총액은 20~50프로 떨어졌다. 새로 메타버스의 유행을 이끈 Meta 기업에서 이렇다 할 결과물이 나오지 않고 메타버스 결부 벤처기업인 '리얼리티 랩스'에 투자한 150억 불 이상을 지출했지만 어떻게 사용됐는지 밝히고 있지 않고 있다. 반면에 대개 시장의 성장 가능성에 관해서 긍정적으로 바라보고 있다. 딜로이트는 메타버스 시장 규모가 전재 조준 1220억 달러에서 2025년 3000억 수준의 성장을 보여주리라 전망하며 블룸버그 인텔리전스는 2020년 4748억 달러에서 2024년 7833억 달러로, 모간스탠리와 골드만삭스는 메타버스가 sns와 스트리밍, 싸움 플랫폼 등을 대체하며 8조 달러 시장으로 성장할 것이라 기대했다.

 

 국내외 기업들은 메타버스 플랫폼 및 AR, VR 기술을 스마트폰 이상의 속 기술로 여기고 투자를 확대하고 있다. 글로벌 낯짝 IT 기업인 구글, 애플, 메타, MS는 지난 2020년부터 최근까지 집중적으로 가상현실 기술을 보유한 기업들을 인수하며 공격적인 투자를 하고 있다.

 

 메타버스 로드맵을 보면, 업계에서는 현재를 메타버스 태동기로 보고 있으며 연결 기술이 점점 고도화되고 개념이 강화되며 향후 5~10년 안에 초기단계의 메타버스로 진입할 것으로 전망했다.

 

## 메타버스 바람 (7 trends that will shape the future of the metaverse in 2023)
 1. Hype will die down
 전문가들이 예상한 2023년의 주인 눈에 띄는 변화는 메타버스의 애매한 컨셉이 사라진다는 것이다.  Jean-Francois Bobier of Boston Consulting Group에 따르면 메타버스에서 가능하리라 생각했던 것들이 무너지며 메타버스 명제가 더한층 명확해질 것이라 말했다.

 

 2. Metaverse as a feature not product
 IT 회사인 Forrester은 소비자들은 메타버스를 feature로 경험할 것이라 말했다. 예를 접어들다 MS는 Teams에 3D 아바타를 도입을 경계 것을 언급할 요체 있다. - 모씨 메타버스 = AR/VR 기기를 떠오르기 십상인데 이러한 생각이 메타버스 = 3D 아바타 또 3D 월드로 바뀌는 것을 의미한 것 같다.
 

 3. Rise of the corporate metaverse

 

 BCG(Boston Consulting Group)에 따르면 메타버스의 인기를 높일 운 있는 두가지의 메인 유스케이스가 있다고 말한다.

 [메타버스](https://silkunequaled.com/life/post-00040.html){:rel="nofollow"} 1. Digital twin
 디지털 트윈은 의의 그냥 실세계를 복사하는 것인데 이식 기술은 IoT와 밀접하게 관련이있다. 아울러 사람들은 디바이스 센서에서 나오는 데이터를 음성 꼼꼼 볼 고갱이 있다. 이는 기업들이 복잡한 데이터를 엄청나게 간단하고 효과적으로 시각화할 수 있다는 것을 있다는 것을 의미한다.
 

 2. Training
 AR/VR 헤드셋을 통해서 가설 트레이닝 프로그램을 할 복운 있다. Neurological의 연구 결과에 따르면 AR/VR을 통해서 트레이닝을 하면 썩 가나오나 기억할 복 있으며, 유달리 physical safety관련 직업에서 유용하게 사용할 생목숨 있다.
 

 4. 5G meets the metaverse

 5G가 가지고 있는 초밀집, 저지연 네트워크를 통해 메타버스의 잠재력을 일절 사용할 수있을 것이라고 Vertiv는 예상을 했다.

 

 5. Payments in the metaverse
 암호화폐의 쇠락에도 불구하고 wed 3는 차츰차츰 커지고 있으며 milions of 유로는 nft, 디지털 자산의 거래는 블록체인을 통해 계속해서 거래되고 있다. 동시에 gatekeeper는 이러한 거래에 높은 수수료를 부과하고 있다. (Gatekeeper: The Unifying Framework is a value‑based self‑regulatory framework for private sector intermediaries who are strategically positioned to prevent or interrupt illicit financial flows – collectively referred to as “gatekeepers”) International law firm bird & bird는 금융 기업이 이러한 거래에서 기회를 얻을 핵 있다고 본다.

 

 6. Metaverse as a marketing channel

 Z세대는 메타버스의 첫번째 유저이자 독 중요한 유저라 할 생명 있다. BCG에 따르면 기이 많은 z세대는 페이스북, 인스타그램을 넘어서 proto-metaverse에 많은 시간을 쏟고 있다. 이는 새로운 마케팅 채널로 볼 핵심 있고 몇몇 기업은 메타버스를 무대로 마케팅 홍보를 시작했다.

 

 7. Need for updated security
 web 2.0과 마찬가지로 남편 중요한 아젠다는 과연 보안이다. 메타버스의 성장을 보안이 따라잡을 수 없는 상황이며, VMware는 이러한 부분에 경고를 했다. 유난히 메타버스는 초엽 단계에 있음으로 기업들은 일층 보안에 집중해야 한다.
 

## 햅틱 기술
 메타버스의 성장에 따라 많은 기술들이 주목받고 있다. 그중 현시대 설명할 것은 햅틱 기술이다. 완전한 몰입형 메타버스를 만들기 위해서는 현실감 향상이 위선 과제인데 이를 햅틱 기술로써 표현할 수 있다. 햅틱 기술이란 관능 경험을 강화하는 기술인데, 햅틱 장치를 이용한 시뮬레이션은 가상현실 속에 존재하는 물체와 상호작용을 할 끼 물리적인 촉각적 느낌을 사용자에게 전달하여 기필코 실지 환경과 상호작용하는 것처럼 물적 느낌을 전달하는 것을 목적으로 한다.

 

 햅틱 기술은 이미 게임, 로봇, 우주, 승용차 산업, 치료 등등에서 널리 사용하고 있으며 계속해서 성장을 하고 있다. 메타버스에서의 햅틱 기술의 성숙도가 올라가면 우리는 참것 가상세계로 이주를 준비하고 있지 않을까 생각해 본다.

 

 출처:
 http://webzine.koita.or.kr/201210-culture/Hot-Agenda-%EA%B0%80%EC%83%81%EC%9D%98-%ED%98%84%EC%8B%A4%EC%9D%84-%EB%8D%94%EC%9A%B1-%EC%8B%A4%EA%B0%90%EB%82%98%EA%B2%8C-%ED%96%85%ED%8B%B1-%EA%B8%B0%EC%88%A0%EC%9D%98-%EC%A7%84%ED%99%94
 https://biz.chosun.com/distribution/channel/2022/10/25/2XAWZ5AQMVFFDKXIMRBHK6AFWI/
 https://jmagazine.joins.com/forbes/view/336722
 https://www3.weforum.org/docs/WEF_Gatekeepers_A_Unifying_Framework_2021.pdf
 https://www.siliconrepublic.com/business/metaverse-hype-trends-artificial-virtual-reality-2023
 

 

