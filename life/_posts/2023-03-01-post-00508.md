---
layout: post
title: "모범택시 시즌2 11회 줄거리"
toc: true
---

 모범택시 시즌2 11회에서 무지개 운수 멤버들은 김도기를 노리는 온하준의 정체를 파헤친다. SBS 금토드라마 모범택시 시즌2 11회는 2023년 3월 31일(금) 오후 10시에 방송된다.↯- 미리보기/예고/줄거리/TV/모범택시 시즌2 😆

## 모범택시 시즌2 11회 줄거리
 지난 3월 25일(토) 방송된 모범택시 시즌2(극본: 오상호, 연출: 이단) 10회에서 공수호(김재민) 과장의 대행 수술을 알아낸 김도기(이제훈)는 산업 과장을 납치해 한수련(강설) 중관 사건의 자백을 받아냈다. 원부부장(김용진)을 초죽음으로 만든 도기는 무지개 운수 멤버들을 소집했다.
 멤버들은 위중한 환자를 [강남클럽](https://shearbray.com/life/post-00043.html) 이송시키고, 서류를 위조해 안영숙(이항나) 원장의 전 재산을 기부했다. 공 과장은 자수를 하고, 각시 원장은 의료사고 건으로 체포됐다. 하지만, 요조숙녀 원장은 또 다시금 과 처럼 개명을 하고 개원할 계획을 세웠다. 결국, 도기는 부 원장을 수술대 위에 눕혔다.
 한편, 다른 병원에서 치료를 받던 한수련 환자가 깨어났다. 하지만, 운행을 종료하고 무지개 운수로 돌아가던 도기와의 통신이 끊기고 모범택시가 폭발했다. 31일(토) 신보 예정인 모범택시 시즌2 11회에서 자신의 죽음을 위장한 김도기는 온하준의 정체에 다가간다.

 경구 : 말도 댁네 돼. 김도기 기사가 죽다니.

 하준 : 이익 오합지졸들 뒤에 누군가 있다면 행동대장이 죽었는데, 모 식으로든 움직이지 않겠어요?

 성철 : 누군가 우리를 사뭇 싫어한다.

 고은 : 김도기 기사님 장례식에 왔던 낯선 사람들 신원 나왔어요. 싹 여기서 근무 중이에요.

 경구 : 블랙썬. 강남의 이녁 유명한 클럽

 진언 : 클럽에서 김도기 기사를 호위호 노려?

 하준 : 제한 명함 보다 죽여볼까?

 하준 : 오래 사세요. 퍽 무리하지 마시고요.

 진언 : 은기사가 김도기 기사를 죽이려고 했다는 거예요?

 성철 : 처음부터 다름 목적을 가지고 접근한 거였어.

 도기 : 친구 안사람 하길 잘했네요.

 도기 : 현금 움직일 때가 된 것 같은데요.

 시청률 조사회사 닐슨코리아에 따르면 지난 3월 24일(금) 방송된 SBS 금토드라마 모범택시 시즌2 10회 시청률은 4.3% 상승한 17.7%를 기록하며 모범택시 시즌2 마음가짐 최상 시청률을 경신했다.
 베일에 가려진 택시회사 무지개 운수와 택시기사 김도기가 억울한 피해자를 대신해 복수를 완성하는 문화물 요행 대행극, SBS 모범택시 시즌2 12회는 2023년 4월 1일(토) 오후 10시에 방송된다.

### 모범택시 시즌2 몇부작
 SBS 금토드라마 모범택시 시즌2는 2023년 2월 17일(금)부터 4월 8일(토)까지 12부작으로 방송 예정이다. 모범택시 시즌2는 웨이브와 쿠팡플레이에서 스트리밍 한다.

#### 모범택시 시즌2 등장인물
 모범택시 시즌2 11회 골프채 블랙썬 MD 윈디 역으로 김채은 배우가 출연한다. 윈디는 죄의식 없이 악행을 저지르는 캐릭터로 돈이 되는 모든 나쁜 짓들을 일삼는 빌런이다.

#### 모범택시 시즌2 인물관계도

 KBS1 일일드라마 금이야 옥이야 | 싱글대디 금강산과 입양아 옥미래를 중심으로 금이야 옥이야 자식을 키우며 성장해 가는 가족들의 유쾌하고 도량 찡한 언급 - 3월 27일(월) 첫 방송
 넷플릭스 길복순 | 청부살인업계의 전설적인 킬러 길복순이 회사와 재계약 직전, 죽거나 혹은 죽이거나, 피할 길운 없는 대결에 휘말리게 되는 이야기를 그린 액션 영화 - 3월 31일(금) 공개
