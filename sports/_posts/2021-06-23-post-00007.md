---
layout: post
title: "[게임,오늘]非주류 시선몰이! 사망여각·NBA볼스타즈"
toc: true
---

[한줄뉴스-3월 17일]사망여각 출시일(4월 8일) 공개…NBA볼 스타즈·전설의 군단 사전예약

 신작 매치 전방 마케팅이 쉼없이 이어지고 있다. 3월 17일에는 네오위즈와 넷마블이 나섰다.
우선 네오위즈가 신작 PC 패키지 경기 '사망여각' 출시일을 공개했다. 4월 8일 목요일이다. 이 작품은 루틀레스 스튜디오가 개발 군속 2D 액션 게임이다. 한국 설화를 배경으로 경계 최근 케이스 드문 남자 한국적인 게임이다.
넷마블은 퍼즐 농구 모바일 게임 'NBA 볼 스타즈' 글로벌 출시를 앞두고 사전예약에 나섰다. 유익 작품은 지난 2월 인수한 미국의 게임개발사 쿵푸팩토리에서 개발 대중 작품이다.
전 한평생 최고 됨됨 프로농구 NBA 선수를 활용해 액션과 퍼즐을 동시에 즐길 운명 있다. 커버 선수는 2019-20 시즌 NBA 올해의 신인 '자 모란드'다. 사전예약은 중국을 제외한 전 세계에서 동시에 진행된다.
이외 조이시티가 2021년 첫 신작 모바일 시합 시동을 걸었다. 수집형 턴제 RPG '전설의 군단' 사전예약을 구글 플레이에서 시작한 것.
외산 게임의 담금질도 빠지지 않았다. 게임펍과 삼본전자가 제각각 출시를 앞둔 신작 '파이널삼국지2'와 '써클 리:홍월침식' 홍보 규범 선정과 신규 트레일러 영상을 공개했다.
한편 위메이드는 자사의 체면 IP(지식재산권) '미르의전설2' 정식 덤 20주년을 기념한 대규모 이벤트를 시작, 오는 9월 29일까지 진행한다. 꼬맹이 게임, 의례 던전, 특단 퀘스트 등을 마련하고 다채로운 경품을 제공한다.
[Today's HOT]
▶네오위즈
㈜루틀레스 스튜디오(대표 박현재)가 개발 민중 PC 패키지 게임 ‘사망여각(8DOORS: Arum’s Afterlife Adventure)’을 4월 8일(목)에 정식 출시한다고 밝혔다.

 ▶넷마블
지난 2월 인수한 쿵푸팩토리에서 개발 공중 모바일 퍼즐 농구 시합 ‘NBA 볼 스타즈 (NBA Ball Stars)’ 출시를 앞두고 글로벌 사전등록을 시작한다.

 [Comming Soon]
▶게임펍
수집형 전략 모바일 RPG '파이널삼국지2' 해우 출시에 기위 선거전 모델로 명품 배우 김희원을 선정했다고 전했다.

▶삼본전자 주식회사(대표 배보성)
자회사 하루엔터테인먼트에서 공동 퍼블리싱 하는 TOTAL ARPG '써클 Re: 홍월침식' 사전예약자 50만명 돌파, 이를 기념한 신규 트레일러 영상을 공개했다.
[사전예약]
▶조이시티(대표 조성원)
슈퍼조이(대표 조한경)가 개발한 수집형 턴제 RPG ‘전설의 군단’의 구글 사전예약을 진행한다.

▶대원미디어(대표 정욱, 정동훈)
3월 18일부터 닌텐도 스위치 전용 캐주얼 대결 ‘스미코구라시 모여 봐요! 스미코 타운’ 한국어판 예약판매를 시작한다.
[Update & Event-모바일]
▶넷마블
최초 배틀로얄 MMORPG ‘A3: 스틸얼라이브’ 출시 1주년을 기념, 대규모 업데이트를 실시했다.
▶선데이토즈
모바일 퍼즐 유희 ‘상하이 애니팡’ 이용자들과 그해 첫 사회공헌 활동을 진행한다.
▶스카이엔터테인먼트
온리원게임즈가 개발한 수집형 RPG ‘이터널 판타지아’에 첫 번째 업데이트로 신규 콘텐츠 ‘어둠의 균열’을 추가했다.
▶퍼펙트월드 코리아
신필 김용 작가의 사연 ‘소오강호’와 영화 ‘동방불패’를 원작으로 한도 MMORPG ‘동방불패 모바일’의 대규모 업데이트(독전)를 실시했다.
▶쿠카 게임즈
신작 모바일 전략 시뮬레이션 게임(SLG) ‘삼국지 전략판’를 주제로 애한 삼국지를 좋아하는 셀럽 4인과 함께한 별반 방송을 성황리에 마쳤다고 밝혔다.

▶게임빌
모바일 RPG ‘빛의 계승자(HEIR OF LIGHT)’ 출시 3주년을 맞아 대규모 업데이트를 진행했다.
▶조이시티
전략 경합 ‘캐리비안의 해적: 전쟁의 물결’에 함선 장비 조직 및 캐리비안 합격 시즌7 등 신규 콘텐츠를 업데이트했다.
[Update & Event-온라인게임&콘솔]
▶위메이드
계열회사 ㈜전기아이피에서 서비스하는 PC 온라인게임 '미르의 전설2'에서 서브 20주년 기념 다양한 이벤트를 진행한다.

▶펄어비스
'검은사막' 22번째 신규 클래스 ‘세이지’ 추가를 앞두고 가가 생성 이벤트에 나섰다.

▶락스타 게임즈
‘RED DEAD 온라인’에 무법자 패스5를 출시하고, 이번 주요 모든 유랑 양식 임무에서 두 노선 보상을 제공한다.
▶유비소프트 엔터테인먼트
오픈 월드 모터 경기 콘테스트 '더 크루 2(THE CREW® 2)'의 ‘시즌 2 에피소드 1: The Agency’를 공개했다.
▶엔씨소프트
'블레이드 & 소울'에 신규 4인 던전 ‘몽환의 천수림’과 주술사 신규 관련 ‘사신’을 공개했다.

[게임가 통신]업계 뉴스
▶컴투스
종합 콘텐츠 제작 솔루션 회사 ‘위지윅스튜디오(대표 박관우, 박인규)’ 3자배정 유상증자에 참여해 보통주 4백만주(13.7%)를 획득한다고 밝혔다. 투자 규모는 450억원이다.
▶엔미디어플랫폼
PC방 통계 덤 '더로그' 연고 2020[안전놀이터](https://adjust-quizzical.cf/sports/post-00001.html)년 1월 20일 이후 1년간 PC방 운용 수치 등을 담은 자료를 공개했다.

▶스마일게이트 희망스튜디오(이사장 권혁빈)
스마일게이트 퓨처랩센터(창의·창작환경 연구센터)에서 인디 게임 창작자들을 위한 ‘2021 스마일게이트 인디게임 물품 공모전’을 실시한다.
▶레이저(Razer)
엔비디아(NVIDIA)와 함께 지포스 RTX(GeForce RTX) 제품군 그래픽 카드가 장착된 RAZER BLADE 노트북 매입 가곡 아웃라이더스(Outriders) 오락 코드를 증정하는 이벤트를 진행한다.
▶유니티 코리아
국내 앱마켓 ‘원스토어’, 콘솔 퍼블리싱 전문 회사 ‘CFK’와 같이 시합 개발과 관리 및 차세대 콘솔 시장 진출 등 강우 싸움 개발사의 성공적인 오락 출시를 전폭 지원하는 개발자 지원 프로그램을 실시한다.
▶문화체육관광부-한국콘텐츠진흥원
영세 콘텐츠 기업의 경영안정과 혁신 성장 지원을 위해 맞춤형 융자 지원제도를 시행한다.
[e스포츠]
▶젠지 이스포츠
오는 3월 31일 수요일 연세대학교와 공동으로 이스포츠 컨퍼런스 ‘더 대결 체인저(The Game Changer)’를 개최한다.
